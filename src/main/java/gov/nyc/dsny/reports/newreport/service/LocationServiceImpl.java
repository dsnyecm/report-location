package gov.nyc.dsny.reports.newreport.service;

import gov.nyc.dsny.reports.common.domain.referencedata.personnel.MdaType;
import gov.nyc.dsny.reports.newreport.entity.Location;
import gov.nyc.dsny.reports.newreport.entity.WorkUnit;
import gov.nyc.dsny.reports.newreport.feign.ReferenceDataClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LocationServiceImpl implements LocationService{
    @Autowired
    ReferenceDataClient referenceDataClient;

    @Override
    public List<Location> getAllLocations() {
        List<Location> locationList= new ArrayList<>();

        referenceDataClient.getLocations()
                .stream()
                .forEach(l->{
                    Location location =new Location();
                    location.setCode(l.getCode());
                    location.setDescription(l.getDescription());
                    location.setSortSequence(l.getSortSequence());
                    locationList.add(location);
                });
        return locationList;
    }

    @Override
    public List<Location> getLocationForWorkUnit(String workUnit) {
        List<Location> locationList= new ArrayList<>();

        referenceDataClient.getLocations()
                .stream()
                .filter(l -> l.getWorkUnit() != null && l.getWorkUnit().getCorrelation().equals(workUnit))
                .forEach(l -> {
                    Location location =new Location();
                    location.setCode(l.getCode());
                    location.setDescription(l.getDescription());
                    location.setSortSequence(l.getSortSequence());
                    locationList.add(location);
                });
        return locationList;
    }

    @Override
    public List<WorkUnit> getWorkUnits() {
        List<WorkUnit> workUnitList = new ArrayList<>();
        referenceDataClient.getWorkUnits()
                .stream()
                .forEach(w->{
                    WorkUnit workUnit = new WorkUnit();
                    workUnit.setCode(w.getCorrelationId().getCorrelation());
                    workUnit.setDescription(w.getDescription());
                    workUnit.setSortSequence(w.getSortSequence());
                });
        return workUnitList;
    }

    @Override
    public List<MdaType> getMdaTypes() {
        return referenceDataClient.getMdaTypes();
    }
}
