package gov.nyc.dsny.reports.newreport.feign;

import gov.nyc.dsny.reports.common.domain.referencedata.Location;
import gov.nyc.dsny.reports.common.domain.referencedata.WorkUnit;
import gov.nyc.dsny.reports.common.domain.referencedata.personnel.MdaType;
import gov.nyc.dsny.reports.newreport.configuration.FeignClientsConfiguration;
import gov.nyc.dsny.reports.newreport.entity.DeviceDownCode;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name="reference-data",configuration = FeignClientsConfiguration.class,url ="${feign.url}")
@RibbonClient(name="reference-data")
public interface ReferenceDataClient {

    @RequestMapping(value="referencedata/deviceDownCodes", produces = MediaType.APPLICATION_JSON_VALUE,method = RequestMethod.GET)
    List<DeviceDownCode> getDeviceDownCodes();

    @RequestMapping(value="referencedata/workUnits",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    List<WorkUnit> getWorkUnits();

    @RequestMapping(value = "referencedata/locations", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Location> getLocations();

    @RequestMapping(value="referencedata/mdaTypes",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    List<MdaType> getMdaTypes();

}
