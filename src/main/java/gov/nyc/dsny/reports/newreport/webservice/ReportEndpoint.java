package gov.nyc.dsny.reports.newreport.webservice;


import gov.nyc.dsny.reports.common.domain.referencedata.personnel.MdaType;
import gov.nyc.dsny.reports.newreport.entity.DeviceDownCode;
import gov.nyc.dsny.reports.newreport.entity.Location;
import gov.nyc.dsny.reports.newreport.entity.WorkUnit;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

/**
 *
 */
@Component
@WebService(serviceName="ReportEndpoint", targetNamespace="http://report.reports.dsny.nyc.gov/")
public interface ReportEndpoint {

    @WebMethod(operationName = "getWorkUnits", action = "{http://locations.reports.dsny.nyc.gov/}getWorkUnits")
    @WebResult(name = "workUnit", partName = "workUnit")
    List<WorkUnit>  getWorkUnits(@WebParam(name = "queryDate", targetNamespace = "http://locations.reports.dsny.nyc.gov/") String queryDate);

    @WebMethod(operationName = "getAllLocations", action = "{http://locations.reports.dsny.nyc.gov/}getLocations")
    @WebResult(name = "allLocation", partName = "allLocation")
    List<Location> getAllLocations(@WebParam(name = "queryDate", targetNamespace = "http://locations.reports.dsny.nyc.gov/") String queryDate);

    @WebMethod(operationName = "getLocationsForWorkUnit", action = "{http://locations.reports.dsny.nyc.gov/}getLocations")
    @WebResult(name = "locationForWorkUnit", partName = "allLocation")
    List<Location> getLocationsForWorkUnit(@WebParam(name = "queryDate", targetNamespace = "http://locations.reports.dsny.nyc.gov/") String queryDate,@WebParam(name = "workUnit", targetNamespace = "http://locations.reports.dsny.nyc.gov/") String workUnit);

    @WebMethod(operationName = "getMdaTypes", action = "{http://locations.reports.dsny.nyc.gov/}getLocations")
    @WebResult(name = "mda", partName = "mda")
    List<MdaType> getMdaTypes(@WebParam(name = "queryDate", targetNamespace = "http://locations.reports.dsny.nyc.gov/") String queryDate);

    @WebMethod(operationName = "getShopWorkUnits", action = "{http://locations.reports.dsny.nyc.gov/}getLocations")
    @WebResult(name = "shopWorkUnits", partName = "shopWorkUnits")
    List<WorkUnit> getShopWorkUnits(@WebParam(name = "queryDate", targetNamespace = "http://locations.reports.dsny.nyc.gov/") String queryDate);

    @WebMethod(operationName = "getShopLocationsForWorkUnit", action = "{http://locations.reports.dsny.nyc.gov/}getLocations")
    @WebResult(name = "shopLocations", partName = "shopLocations")
    List<Location> getShopLocationsForWorkUnit(@WebParam(name = "queryDate", targetNamespace = "http://locations.reports.dsny.nyc.gov/") String queryDate);



}