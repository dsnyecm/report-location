package gov.nyc.dsny.reports.newreport.entity;

import gov.nyc.dsny.smart.common.jackson.reference.MdaTypeId;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Location")
public class Location {
    private String code;
    private String description;
    private Integer sortSequence;
}
