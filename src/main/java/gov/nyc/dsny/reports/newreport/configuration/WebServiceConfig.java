package gov.nyc.dsny.reports.newreport.configuration;

import gov.nyc.dsny.reports.newreport.webservice.ReportEndpoint;
import gov.nyc.dsny.reports.newreport.webservice.MockReportEndpointImpl;
import gov.nyc.dsny.reports.newreport.webservice.ReportEndpointImpl;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 * Created by ksafronov on 8/22/2016.
 */
@Configuration
public class WebServiceConfig {

    @Bean
    public ReportEndpoint newreport() {
        return new MockReportEndpointImpl();
    }

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), newreport());
        endpoint.publish("/locations");
        return endpoint;
    }

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

}
