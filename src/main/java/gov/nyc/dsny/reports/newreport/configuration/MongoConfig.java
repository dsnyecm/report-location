package gov.nyc.dsny.reports.newreport.configuration;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import gov.nyc.dsny.reports.common.repos.referencedata.base.ReferenceDataRepositoryImpl;
import org.dsny.dal.refdata.RefData;
import org.dsny.dal.refdata.RefDataImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@Configuration
@EnableMongoRepositories(basePackages = {"gov.nyc.dsny.reports.common.repos.referencedata"},
        repositoryBaseClass = ReferenceDataRepositoryImpl.class, mongoTemplateRef = "smartMongo")
public class MongoConfig {

    private static final String EQUIPMENT_DB = "equipment";
    private static final String PERSONNEL_DB = "personnel";
    private static final String TASK_DB = "task";
    private static final String REFERENCE_DATA = "reference-data";

    @Autowired
    private Environment environment;

    @Value("#{environment['ENV_DW_MONGO_CONN_STRING'] ?: 'mongodb://webServ:webServ@10.155.208.96:37017'}")
    private String mongoUri;

    @Value("#{environment['ENV_SMART_MONGO_CONN_STRING'] ?: 'mongodb://10.155.208.107:27017'}")
    private String smartMongoUri;

    // mongo client for reports databases
    @Bean
    public MongoClient mongoClient() throws Exception {
        return new MongoClient(new MongoClientURI(mongoUri));
    }
    // mongo client for SMART database
    @Bean
    public MongoClient mongoClientSmart() throws Exception {
        return new MongoClient(new MongoClientURI(smartMongoUri));
    }


    // equipment mongo config beans
    @Bean
    public MongoDbFactory mongoDbFactoryEquipment() throws Exception {
        return new SimpleMongoDbFactory(mongoClient(), EQUIPMENT_DB);
    }
    @Bean
    public MongoTemplate mongoTemplateEquipment() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactoryEquipment());
        return mongoTemplate;
    }


    //personnel mongo config beans
    @Bean
    public MongoDbFactory mongoDbFactoryPersonnel() throws Exception {
        return new SimpleMongoDbFactory(mongoClient(), PERSONNEL_DB);
    }
    @Bean
    public MongoTemplate mongoTemplatePersonnel() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactoryPersonnel());
        return mongoTemplate;
    }



    //task mongo config beans
    @Bean
    public MongoDbFactory mongoDbFactoryTask() throws Exception {
        return new SimpleMongoDbFactory(mongoClient(), TASK_DB);
    }
    @Bean
    public MongoTemplate mongoTemplateTask() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactoryTask());
        return mongoTemplate;
    }


    // new ref-data mongo config with model/repositories that lives in reports-common
    @Bean
    public MongoDbFactory mongoDbFactorySmartMongo() throws Exception {
        return new SimpleMongoDbFactory(mongoClientSmart(), REFERENCE_DATA);
    }
    @Bean(name = "smartMongo")
    public MongoTemplate mongoTemplateSmartMongo() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactorySmartMongo());
        return mongoTemplate;
    }

    @Bean
    public RefData refData() throws Exception {
        return new RefDataImpl(mongoClientSmart().getDatabase(REFERENCE_DATA));
    }

}