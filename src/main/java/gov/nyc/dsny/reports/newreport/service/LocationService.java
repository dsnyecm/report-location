package gov.nyc.dsny.reports.newreport.service;

import gov.nyc.dsny.reports.common.domain.referencedata.personnel.MdaType;
import gov.nyc.dsny.reports.newreport.entity.Location;
import gov.nyc.dsny.reports.newreport.entity.WorkUnit;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface LocationService {
    List<Location>  getAllLocations();
    List<Location>  getLocationForWorkUnit(String workUnit);
    List<WorkUnit>  getWorkUnits();
    List<MdaType> getMdaTypes();

}
