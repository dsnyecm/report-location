package gov.nyc.dsny.reports.newreport.webservice;


import gov.nyc.dsny.reports.common.domain.referencedata.personnel.MdaType;
import gov.nyc.dsny.reports.newreport.entity.Location;
import gov.nyc.dsny.reports.newreport.entity.WorkUnit;
import gov.nyc.dsny.reports.newreport.feign.ReferenceDataClient;
import gov.nyc.dsny.reports.newreport.entity.DeviceDownCode;
import gov.nyc.dsny.reports.newreport.service.LocationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ksafronov on 8/24/2016.
 */
@Slf4j
public class MockReportEndpointImpl implements ReportEndpoint {

    @Autowired
    LocationService locationService;

    @Override
    public List<WorkUnit> getWorkUnits(String queryDate) {
        return locationService.getWorkUnits();
    }

    @Override
    public List<Location> getAllLocations(String queryDate) {
        return locationService.getAllLocations();
    }

    @Override
    public List<Location> getLocationsForWorkUnit(String queryDate, String workUnit) {
        return locationService.getLocationForWorkUnit(workUnit);
    }

    @Override
    public List<MdaType> getMdaTypes(String queryDate) {
        return locationService.getMdaTypes();
    }
}
