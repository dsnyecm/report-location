# README #

### How do I get set up? ###

* Download as zip file (select download option in left panel)
* Extract and rename folder to your new report name
* Open in intellij and rename package to report name (can also edit each individual file and directory)
* Create new repository in bitbucket under Minerva project
* Follow bitbucket directions to add code to new repo, you may need to issue additional commands: 
git init
git add .
git commit
